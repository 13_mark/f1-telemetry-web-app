import { Component, OnInit } from '@angular/core';
import { RaceDataService } from '../services/race-data.service';
import { JoinedType } from '../race-table/types/types';

@Component({
  selector: 'app-fastes-player-card',
  templateUrl: './fastes-player-card.component.html',
  styleUrls: ['./fastes-player-card.component.scss'],
})
export class FastesPlayerCardComponent {
  fastestRound: JoinedType | undefined;

  constructor(private readonly raceDataService: RaceDataService) {
    this.raceDataService
      .getPlayerWithFastestRound()
      .subscribe((next) => (this.fastestRound = next));
  }
}
