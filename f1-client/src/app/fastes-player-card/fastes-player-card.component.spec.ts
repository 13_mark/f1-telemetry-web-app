import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastesPlayerCardComponent } from './fastes-player-card.component';

describe('FastesPlayerCardComponent', () => {
  let component: FastesPlayerCardComponent;
  let fixture: ComponentFixture<FastesPlayerCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FastesPlayerCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastesPlayerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
