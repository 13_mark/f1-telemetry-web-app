import { Component } from '@angular/core';
import { JoinedType } from './types/types';
import { FlagService } from './services/flag.service';
import { RaceDataService } from '../services/race-data.service';

@Component({
  selector: 'app-race-table',
  templateUrl: './race-table.component.html',
  styleUrls: ['./race-table.component.scss'],
})
export class RaceTableComponent {
  dataSource: JoinedType[] = [];
  fastestRoundPlayer: JoinedType | undefined;

  displayedColumns: string[] = ['position', 'name', 'sector1', 'sector2', 'lastLap', 'bestLap', 'penalties', 'aiDriven'];

  constructor(
    private readonly flagService: FlagService,
    private readonly raceDataService: RaceDataService,
  ) {
    this.raceDataService.getCurrentStanding().subscribe(
      (next) => (this.dataSource = next),
      (err) => console.log(err),
    );
  }

  getClass(nation: number): string {
    return `flag-icon flag-icon-${this.flagService.getFlagName(nation)} margin-right`;
  }

  getPlayerName(nation: number): string {
    return this.flagService.getPlayerName(nation);
  }
}
