import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RaceTableComponent } from './race-table/race-table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MinuteSecondPipe } from './race-table/pipes/minute-second.pipe';
import { FastesPlayerCardComponent } from './fastes-player-card/fastes-player-card.component';
import { MatCardModule } from '@angular/material/card';
import { WaitingScreenComponent } from './waiting-screen/waiting-screen.component';

@NgModule({
  declarations: [
    AppComponent,
    RaceTableComponent,
    MinuteSecondPipe,
    FastesPlayerCardComponent,
    WaitingScreenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
