import { Injectable } from '@angular/core';
import { JoinedType } from '../race-table/types/types';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RaceDataService {
  private readonly myWebSocket: WebSocket = new WebSocket('wss://f1-api.cratory.de');
  private readonly dataSubject = new Subject<JoinedType[]>();

  constructor() {
    this.myWebSocket.addEventListener<'message'>('message', (val: any) => {
      const data: JoinedType[] = JSON.parse(val.data);
      this.dataSubject.next(data);
    });
  }

  getCurrentStanding(): Observable<JoinedType[]> {
    return this.dataSubject
      .asObservable()
      .pipe(
        map((data) => data.sort((a, b) => (a.m_carPosition < b.m_carPosition ? -1 : 1))),
      );
  }

  getPlayerWithFastestRound(): Observable<JoinedType> {
    return this.dataSubject
      .asObservable()
      .pipe(
        map((data) => data.reduce((a, b) => (a.m_bestLapTime < b.m_bestLapTime ? a : b))),
      );
  }
}
