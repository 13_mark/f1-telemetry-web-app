import { Component, OnInit } from '@angular/core';
import { RaceDataService } from './services/race-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  eventStarted = false;

  constructor(private readonly raceDataService: RaceDataService) {
    this.raceDataService.getCurrentStanding().subscribe((next) => {
      if (next.length !== 0) {
        this.eventStarted = true;
      }
    });
  }
}
