import axios from 'axios';
import { F1TelemetryClient, constants } from './telemetry-client/telemtry-client';
import { Participant, LapData, JoinedType } from '../common/types/types';
import { Subject } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
const https = require('https');

const { PACKETS } = constants;
const client = new F1TelemetryClient({ port: 20777 });
const dataSubject = new Subject();
const filtered = dataSubject.pipe(throttleTime(5000));

function mapToJoinedType(participants: Participant[], lapData: LapData[]): JoinedType[] {
  const data = [];
  for (let i = 0; i < participants.length; i++) {
    data.push({ ...lapData[i], ...participants[i] });
  }
  return data;
}

client.on(PACKETS.participants, (participants: { m_participants: Participant[] }) => {
  client.on(PACKETS.lapData, async (lapData: { m_lapData: LapData[] }) => {
    const data = mapToJoinedType(participants.m_participants, lapData.m_lapData);
    dataSubject.next(data);
  });
});

filtered.subscribe(async (next) => {
  const agent = new https.Agent({
    rejectUnauthorized: false,
  });
  await axios.post('https://f1-api.cratory.de/data', next, { httpsAgent: agent });
});

client.start();

[`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `uncaughtException`, `SIGTERM`].forEach(
  (eventType) => {
    (process as NodeJS.EventEmitter).on(eventType, () => {
      console.log('client stopped by', eventType);
      client.stop();
    });
  },
);
